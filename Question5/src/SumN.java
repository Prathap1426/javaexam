	import java.util.Scanner;
public class SumN {

	    public static void main(String[] args) {

	        int num, count, total = 0;
	        System.out.println("Enter N value=");
	        Scanner scan = new Scanner(System.in);
	        num = scan.nextInt();
	        scan.close();
	        for(count = 1; count <= num; count++){
	            total = total + count;
	        }

	        System.out.println("Sum "+num+" natural num: "+total);
	    }
}
